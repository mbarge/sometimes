//
//  ViewController.swift
//  Sometimes
//
//  Created by Matthew Barge on 10/4/17.
//

import UIKit
import Parse

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UITextFieldDelegate {

    @IBOutlet weak var postTextField: UITextField!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sometimesButton: UIButton!
    var sometimesMessages = [SometimesMessage]()
    var filteredSometimesMessages: [SometimesMessage] = []
    var searchActive: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        postTextField.delegate = self
        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getMessageData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // text field
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func textFieldChanged(_ sender: Any) {
    }
    
    @IBAction func textFieldEditingDidEnd(_ sender: Any) {
    }
    
    @IBAction func postButtonTapped(_ sender: Any) {
        let theMessage = PFObject(className:"Sometimes")
        theMessage["message"] = postTextField.text
        theMessage["parentMessageId"] = "Sean Plott"
        theMessage["indentations"] = 2
        theMessage.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                print("The object has been saved.")
                DispatchQueue.main.async {
                    self.sometimesMessages.removeAll()
                    self.getMessageData()
                    self.tableView.reloadData()
                    self.postTextField.text = ""
                }
            } else {
                print("\(error?.localizedDescription)")
            }
        }
        
        postTextField.resignFirstResponder()
    }
    

}

