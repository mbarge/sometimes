//
//  sometimesTableViewCell.swift
//  Sometimes
//
//  Created by Matthew Barge on 10/4/17.
//

import UIKit

class sometimesTableViewCell: UITableViewCell {

        @IBOutlet weak var messageLabel: UILabel!
        @IBOutlet weak var lolCount: UILabel!
        @IBOutlet weak var lolButton: UIButton!
        @IBOutlet weak var replyButton: UIButton!
        @IBOutlet weak var shareButton: UIButton!
        @IBOutlet weak var indentationConstraint: NSLayoutConstraint!
        @IBOutlet weak var lolButtonIndentationConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
        
}

