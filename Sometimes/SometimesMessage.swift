//
//  SometimesMessage.swift
//  Sometimes
//
//  Created by Matthew Barge on 10/5/17.
//

import Foundation

struct SometimesMessage {
    var messageId: String?
    var messageContent: String?
    var parentMessageId: String?
    var indentations: Int?
    var dateCreated: Date?
    var dateUpdated: Date?
}
