//
//  SometimesSearch.swift
//  Sometimes
//
//  Created by Matthew Barge on 11/7/17.
//

import Foundation
import UIKit
import Parse

extension ViewController {
    // api
    func getMessageData() {
        let query = PFQuery(className:"Sometimes")
        //        query.whereKey("playerName", equalTo:"Sean Plott")
        query.order(byDescending: "createdAt")
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        //                        if let theMessageId = object["objectId"] as! String?, let theMessageContent = object["message"] as! String?, let theParentMessageId = object["parentMessageId"] as! String?, let theIndentations = object["indentations"] as! Int?, let theDateCreated = object["createdAt"] as! Date?, let theDateUpdated = object["updatedAt"] as! Date? {
                        let theMessage = SometimesMessage(messageId: object["objectId"] as? String, messageContent: object["message"] as! String?, parentMessageId: object["parentMessageId"] as! String?, indentations: object["indentations"] as! Int?, dateCreated: object["createdAt"] as! Date?, dateUpdated: object["updatedAt"] as! Date?)
                        self.sometimesMessages.append(theMessage)
                        //                        }
                    }
                }
                self.tableView.reloadData()
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.localizedDescription)")
            }
        }
    }
    
    // search bar
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // search every time a character is entered
        if searchBar.text != "" {
            searchActive = true
            filterContentForSearchText(searchText: searchBar.text!)
            tableView.reloadData()
        } else {
            searchActive = false
            tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        // filter messages by any word
        filteredSometimesMessages = sometimesMessages.filter { message in
            return message.messageContent!.lowercased().contains(searchText.lowercased())
        }
    }
}
