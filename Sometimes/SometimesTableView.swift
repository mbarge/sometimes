//
//  SometimesTableView.swift
//  Sometimes
//
//  Created by Matthew Barge on 11/7/17.
//

import Foundation
import UIKit

extension ViewController {
    // tableview
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // tableview data source is based on whether customer is searching or not
        if searchActive {
            return filteredSometimesMessages.count
        }
        return (sometimesMessages.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: "sometimesCell", for: indexPath) as? sometimesTableViewCell)!
        var dataSource: [SometimesMessage] = sometimesMessages
        if searchActive {
            dataSource = filteredSometimesMessages
        }
        cell.messageLabel.text = dataSource[indexPath.row].messageContent
        //        print(indexPath.row)
        //        if indexPath.row == 1 {
        //            cell.messageLabel.text = "Sometimes it's like Cool story, bro"
        //            cell.indentationConstraint.constant = 20
        //            cell.lolButtonIndentationConstraint.constant = 25
        //        } else if indexPath.row == 2 {
        //            cell.messageLabel.text = "Sometimes my shoes come untied at the weirdest times did you know how long it took me to tie them back?"
        //            cell.indentationConstraint.constant = 30
        //            cell.lolButtonIndentationConstraint.constant = 35
        //        }
        return cell
    }
}
